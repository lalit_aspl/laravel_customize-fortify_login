<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\User;

class UsersController extends Controller
{
    public function index()
    {  $data = User::paginate(2);
      //  echo "<pre>"; print_r($data->toArray());
       return Inertia::render('User/Home',compact('data'));
    }

 

    public function delete(Request $request)
    {
        User::where('id' , $request->id)->delete();
        return redirect()->back();
    }
}
