<?php

namespace App\Http\Responses;

use Illuminate\Support\Facades\Auth;
use Laravel\Fortify\Contracts\LoginResponse as LoginResponseContract;

class LoginResponse implements LoginResponseContract
{

    public function toResponse($request)
    {
        
        if (auth()->user()->role_id == 1 && !empty(auth()->user()->role_id)) {
            redirect()->intended(config('fortify.home'));
        }else{
            return redirect('home');
        }
        
    }

}