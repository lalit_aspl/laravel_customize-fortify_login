<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsersController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return Inertia\Inertia::render('Dashboard');
// })->name('dashboard');



// Route::group(['middleware' => ['roles','auth:sanctum', 'verified'], 'roles' => ['admin']], function () {
 
//     Route::get('/dashboard', function () {
//             return Inertia\Inertia::render('Dashboard');
//         })->name('dashboard');
    
  
// });


Route::group(['middleware' => ['roles','auth:sanctum', 'verified'], 'roles' => ['admin']], function () {
 
    Route::get('/dashboard', function () {
            return Inertia\Inertia::render('Dashboard');
        })->name('dashboard');
    
  
});

Route::group(['middleware' => ['roles','auth:sanctum', 'verified'], 'roles' => ['user']], function () {
 
    //$url = action([App\Http\Controllers\UsersController::class, 'index']);

    Route::get('/home', [UsersController::class, 'index'])->name('Home');    
    
    Route::POST('/users/delete/{delete}', [UsersController::class, 'delete']);

    
    
  
});
